/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

/**
 *
 * @author emoru
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import vista.jfBanco;

public class BancoController {
    
    private jfBanco view;

    public BancoController(jfBanco view) {
        this.view = view;

        // Agregar los ActionListener a los botones
        view.btnNuevo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        view.btnGuardar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        view.btnMostrar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });

        view.btnDeposito.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnDepositoActionPerformed(evt);
            }
        });

        view.btnRetiro.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                btnRetiroActionPerformed(evt);
            }
        });
    }

    private void btnNuevoActionPerformed(ActionEvent evt) {
String numeroCuenta = view.txtNumCuenta.getText();
    if (!numeroCuenta.isEmpty()) {
        view.panelDatosCliente.setVisible(true);
        // Resto del código para inicializar los campos del panel Datos de Cliente
    } else {
        view.panelDatosCliente.setVisible(false);
    }
    }

    private void btnGuardarActionPerformed(ActionEvent evt) {
        // Acción a realizar cuando se presiona el botón "Guardar"
        // Aquí puedes agregar la lógica para guardar los datos del cliente en el modelo
    }

    private void btnMostrarActionPerformed(ActionEvent evt) {
        // Acción a realizar cuando se presiona el botón "Mostrar"
        // Aquí puedes agregar la lógica para mostrar los datos del cliente en la vista
    }

    private void btnDepositoActionPerformed(ActionEvent evt) {
        // Acción a realizar cuando se presiona el botón "Hacer depósito"
        // Aquí puedes agregar la lógica para realizar un depósito en la cuenta bancaria
    }

    private void btnRetiroActionPerformed(ActionEvent evt) {
        // Acción a realizar cuando se presiona el botón "Hacer retiro"
        // Aquí puedes agregar la lógica para realizar un retiro de la cuenta bancaria
    }

    public static void main(String[] args) {
        // Crear una instancia del controlador y pasarle la vista como parámetro
        jfBanco view = new jfBanco();
        BancoController controller = new BancoController(view);
        view.setVisible(true);
    }
}