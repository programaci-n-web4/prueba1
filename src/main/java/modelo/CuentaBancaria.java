/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author emoru
 */
// Modelo: CuentaBancaria.java
public class CuentaBancaria {
    private String numeroCuenta;
    private Cliente datosCliente;
    private Fecha fechaApertura;
    private String nombreBanco;
    private float porcentajeRendimiento;
    private float saldo;

    public CuentaBancaria(String numeroCuenta, Cliente datosCliente, Fecha fechaApertura, String nombreBanco,
            float porcentajeRendimiento) {
        this.numeroCuenta = numeroCuenta;
        this.datosCliente = datosCliente;
        this.fechaApertura = fechaApertura;
        this.nombreBanco = nombreBanco;
        this.porcentajeRendimiento = porcentajeRendimiento;
        this.saldo = 0.0f;
    }

    public void depositar(float cantidad) {
        saldo += cantidad;
    }

    public boolean retirar(float cantidad) {
        if (cantidad <= saldo) {
            saldo -= cantidad;
            return true;
        }
        return false;
    }

    public float calcularReditos() {
        return (porcentajeRendimiento * saldo) / 365;
    }

    // Getters y setters para los atributos

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public Cliente getDatosCliente() {
        return datosCliente;
    }

    public void setDatosCliente(Cliente datosCliente) {
        this.datosCliente = datosCliente;
    }

    public Fecha getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Fecha fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public float getPorcentajeRendimiento() {
        return porcentajeRendimiento;
    }

    public void setPorcentajeRendimiento(float porcentajeRendimiento) {
        this.porcentajeRendimiento = porcentajeRendimiento;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
}

